using NUnit.Framework;
using NUnit.Framework.Constraints;
using Student_Management_System.Models;
using System.Collections.Generic;

namespace Test
{
	public class Tests
	{
        static List<Student> StudentList = new List<Student>();//lista studenti statica
        private static Student stud = new Student(null, "Anonimus", "Anonim");

		[SetUp]
		public void Setup()
        {
            StudentList.Add(new Student("1", "Andrei", "Tudor"));
            StudentList.Add(new Student("2", "Mircea", "Alex"));
            StudentList.Add(new Student("3", "Popa", "Popescu"));
            StudentList.Add(new Student("4", "Stefan", "Alexandru"));
            StudentList.Add(new Student(null, "Ciprian", "Bogdan"));
           
        }
		

        Student student = new Student();
        [Test]
		public void FirstTest()
		{
			Assert.That(student, Is.InstanceOf<Student>());
			Assert.Pass();
		}

		[Test]
		[TestCase("Andrei","Tudor", true)]
		[TestCase("Andreiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii","Tudor",true)]
		[TestCase("Nameeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee","Popescu",false)]
		public void FirstName_LastName_CantBeOver64(string Fname, string Lname, bool expectedResult)
		{
			bool result;
			Student student = new Student();
			student.FirstName = Fname;
			student.LastName = Lname;

			if (student.FirstName.Length < 65 && student.LastName.Length < 65)
                result = true;
            else
                result = false;
			
			Assert.AreEqual(expectedResult, result);
		}


		[Test]
		[TestCase("Andrei", "Tudor", true)]
		[TestCase("Andrei", null, false)]
		[TestCase(null,"Tudor",true)]//Aici vom avea un test cu rezulatul Failed
		public void FirstName_LastName_CantBeNull(string Fname, string Lname, bool expectedResult)
		{
			bool result;
			Student student = new Student();
			student.FirstName = Fname;
			student.LastName = Lname;

			if (student.FirstName != null && student.LastName != null)
                result = true;
            else
                result = false;
			
			Assert.AreEqual(expectedResult, result);
		}

        
		
       

        [Test]
        [TestCase("Andrei",true)]
        [TestCase("Tudor",false)]
        public void FirstName_StartsWithCaracter_A(string Fname, bool expectedResult)
        {
            bool result;
            Student student = new Student();
            student.FirstName = Fname;

			if (student.FirstName[0] == 'A')
				result = true;
			else
				result = false;

            Assert.AreEqual(expectedResult, result);

        }

       
        [Test]
        public void UserId_NotNull()
        {
            Student stud = StudentList[3];
            Assert.That(stud.Id, Is.Not.Null);
        }

        [Test]
        public void Distinct_UsersId()
        {
            Student stud = new Student("5", "Alex", "Popescu");//adaugam un student nou si verificam id-ul lui cu restul id-urilor din lista cu studenti
            StudentList.Add(stud);

            bool result = true;
            foreach (Student s in StudentList)
            {
                if (s.Id == stud.Id)
                {
                    result = false;
                    StudentList.Remove(stud);
                }
            }

            Assert.That(result, Is.True);
        }

       
	}
}